# Site perso ADSY

## Installation du projet

* Création du vhost du projet
    ```
    <VirtualHost *:80>
      ServerName adsy.com
      ServerAlias adsy.com
      DocumentRoot "C:/Users/Adsy/PhpstormProjects/site-perso/web"
      <Directory "C:/Users/Adsy/PhpstormProjects/site-perso/web">
        AllowOverride All
        Require local
      </Directory>
    </VirtualHost>
    ```
* Récupération d'une base de donnée
* Modification du /sites/default/settings.php
    ```
    $databases['default']['default'] = array (
      'database' => 'site_perso',
      'username' => 'root',
      'password' => '',
      'prefix' => '',
      'host' => 'localhost',
      'port' => '3306',
      'namespace' => 'Drupal//Core//Database//Driver//mysql',
      'driver' => 'mysql',
    );
    ```

## Git process

....

## Configuration PHP_CodeSniffer et PHP Mess Detector

* Ajouter les path vers PHP_CodeSniffer au chemin suivant
    ```
    Languages & Frameworks ==> PHP ==> Quality Tools
        * PHP_CodeSniffer : /vendor/squizlabs/php_codesniffer/bin/phpcs.bat
        * Mess Detector : /vendor/bin/phpmd.bat

    Editor ==> Inspections ==> PHP ==> Quality tools
        * Cocher les 2 plugins
        * Pour PHP_CodeSniffer ==> Coding standard : Drupal
        * Apply
    ```
* Maintenant le code sera sous-ligné s'il ne respecte pas les standards PHP/Drupal :)

#### ... / phpcs
```
vendor/bin/phpcs --standard=Drupal  --standard=DrupalPractice --extensions=php,module,css web/modules/custom
```
```
vendor/bin/phpcs --standard=Drupal  --standard=DrupalPractice --extensions=php,theme web/themes/custom
```

#### Utilisez le thème et build le css avec SASS

Dans le dossier thème
- ``npm install``
- ``npm install -g sass``
- build le css : ``sass ./web/themes/custom/perso/scss/style.scss ./web/themes/custom/perso/css/style.css``

#### Dump la base de données depuis l'extérieur du Docker
- ``docker exec -i [nom_du_docker] mysqldump -u[user_bdd] -p[password_bdd] [nom_de_la_bdd] > [nom_du_fichier_sql]``
- ``docker exec -i site-perso_mysql_1 mysqldump -uroot -proot site_perso > site_perso.sql``

#### SSL
certbot certonly --apache --agree-tos --no-eff-email -dry-run -d adsy.com --rsa-key-size 4096
