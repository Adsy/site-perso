(function ($, Drupal) {
  Drupal.behaviors.main = {
    attach: function (context, settings) {
      $('.strate-swiper').once('swiper-js').each(function () {
        var swiper = new Swiper('.swiper-' + $(this).attr('id'), {
          slidesPerView: 1,
          pagination: {
            el: '.swiper-pagination',
            dynamicBullets: true,
          },
          loop: true,
          navigation: {
            nextEl: '.swiper-button-next-' + $(this).attr('id'),
            prevEl: '.swiper-button-prev-' + $(this).attr('id'),
          },
        });
      });

      $('.swiper-container').once('swiper-js').each(function () {
        var swiper = new Swiper('.swiper-container', {
          slidesPerView: 1,
          loop: true,
          navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
          },
        });
      });
    }
  };
})(jQuery, Drupal);
