<?php

namespace Drupal\adsy_twig\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\taxonomy\Entity\Term;

/**
 * Plugin implementation of the 'tag_article_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "tag_article_formatter",
 *   label = @Translation("Tag article formatter"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class TagArticleFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      // Implement default settings.
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [
      // Implement settings form.
    ] + parent::settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    // Implement settings summary.

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $tid = $item->get('target_id')->getValue();
      if ($tid) {
        $term = Term::load($tid);
        $elements[$delta] = [
          '#termName' => $term->getName(),
          '#termID' => $term->id(),
        ];
      }
    }

    return $elements;
  }

}
